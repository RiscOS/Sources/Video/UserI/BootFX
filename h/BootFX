/* @file BootFX.h
 *
 * Main header file (and exports) for the BootFX module.
 */

#ifndef BOOTFX_H__
#define BOOTFX_H__

/** Internal error codes.
 */
#define BOOTFX_ERR_MALLOC_FAIL     (0x00)
#define BOOTFX_ERR_NO_SPRITES      (0x01)
#define BOOTFX_ERR_INVALID_SPRITES (0x02)
#define BOOTFX_ERR_NO_JPEG         (0x03)
#define BOOTFX_ERR_BAD_BAR_HANDLE  (0x04)

/** Star command syntax error codes.
 */
#define BOOTFX_ERR_SYN_LOAD  (0xFF)
#define BOOTFX_ERR_SYN_POS   (0xFE)
#define BOOTFX_ERR_SYN_PLOT  (0xFD)
#define BOOTFX_ERR_SYN_FREE  (0xFC)
#define BOOTFX_ERR_SYN_DEBUG (0xFB)

/** The path variable published by this module.
 */
#define BOOTFX_SYSVAR "BootFX$Path"

/** This handle is used by BootCommands to update the default booting progress bar.
 */
#define BOOTFX_MAGIC_HANDLE (0xB007CED5)

/** Some stuff that don't seem to be exported by other components (but really should be).
 */
#ifndef JPEG_Info
#define JPEG_Info (0x49980)
#endif
#ifndef JPEG_PlotScaled
#define JPEG_PlotScaled (0x49982)
#endif
#ifndef ScreenFX_Fade
#define ScreenFX_Fade (0x58440)
#endif
/** Global/ModHand.h defines this, but also defines macros that conflict with our CMHG handler functions
 */
#ifndef ModHandReason_LookupName
#define ModHandReason_LookupName (18)
#endif

#endif

/* SWI names from auto-generated CMHG file are appended here in the export_hdrs phase */
